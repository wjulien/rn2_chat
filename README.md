# RN2_CHAT

React native project for Epitech
[CHECK IT ON EXPO](https://expo.io/@wjulien/rn2_chat)

# Table of content

1. How to launch
1. How to lint
1. How to use

# How to launch

```shell
npm start
```

# How to lint

This will check for lint errors
```shell
npm run lint
```

This will try to auto fix what is auto fixable
```shell
npm run lint-fix
```

# How to use

Head to the signup page, enter your informations and press join us. Go back to home screen and log in with you newly created ids.
Chat to your heart's content, and change channel whenever you want by swiping the screen from left to right (and right to left to close it). Alternatively, you can click the top left icon.
