import React from 'react';
import { Provider } from 'react-redux';
import {
  createStackNavigator,
  createAppContainer,
} from 'react-navigation';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { ReactReduxFirebaseProvider } from 'react-redux-firebase';
import { createFirestoreInstance } from 'redux-firestore';

import LoginScreen from './lib/containers/Login';
import SignupScreen from './lib/containers/Signup';
import ChatScreen from './lib/containers/Chat';
import store from './lib/store';

const fbConfig = {
  apiKey: 'AIzaSyAN4boKVdC7-5GV6_IhKuWKgAWAnTfLWLk',
  authDomain: 'rn2chat.firebaseapp.com',
  databaseURL: 'https://rn2chat.firebaseio.com',
  projectId: 'rn2chat',
  storageBucket: 'rn2chat.appspot.com',
  messagingSenderId: '186075095063',
};

const rrfConfig = {
  userProfile: 'users',
  useFirestoreForProfile: true,
};

const RootStack = createStackNavigator({
  Login: LoginScreen,
  Signup: SignupScreen,
  Chat: ChatScreen,
}, {
  initialRouteName: 'Login',
});

if (!firebase.apps.length) {
  firebase.initializeApp(fbConfig);
}

const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  createFirestoreInstance,
};

const AppContainer = createAppContainer(RootStack);

export default () => (
  <Provider store={store}>
    <ReactReduxFirebaseProvider {...rrfProps}>
      <AppContainer />
    </ReactReduxFirebaseProvider>
  </Provider>
);
