import React from 'react';
import {
  StatusBar,
  View,
  FlatList,
  TouchableHighlight,
  PanResponder,
} from 'react-native';
import { connect } from 'react-redux';
import { LinearGradient } from 'expo';
import { Icon } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PropTypes from 'prop-types';
import { compose } from 'redux';

import { firebaseConnect } from 'react-redux-firebase';
import Menu from '../../components/Menu';
import Keyboard from '../../components/KeyBoard';
import Message from '../../components/Message';
import style from './ChatStyle';

class Chat extends React.PureComponent {
  constructor(props) {
    super(props);
    const { Firebase } = props;
    this.state = {
      user: (Firebase.profile) ? Firebase.profile.uid : '',
      pseudo: (Firebase.profile) ? Firebase.profile.pseudo : '',
      channels: (Firebase.profile) ? Firebase.profile.channels : [],
      currentChannel: (Firebase.profile) ? Firebase.profile.currentChannel : '',
      avatar: (Firebase.profile) ? Firebase.profile.avatar : '',
      showMenu: false,
      hideIcon: false,
      chanIsSetup: false,
      userList: [],
    };
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (evt, gestureState) => {
        const { showMenu } = this.state;
        if (!showMenu && (gestureState.x0 < 200 && gestureState.moveX > 170)) {
          this.toggleMenu();
        } else if (showMenu && (gestureState.x0 >= 300 && gestureState.moveX > 140)) {
          this.toggleMenu();
        }
      },
    });
  }

  componentWillMount() {
    const { Firebase } = this.props;
    this.setState({
      avatar: Firebase.profile.avatar ? Firebase.profile.avatar : {},
      user: Firebase.profile.uid ? Firebase.profile.uid : '',
      pseudo: Firebase.profile.pseudo ? Firebase.profile.pseudo : '',
      channels: Firebase.profile.channels ? Firebase.profile.channels : [],
      currentChannel: Firebase.profile.currentChannel ? Firebase.profile.currentChannel : '',
    });
  }

  componentDidUpdate() {
    const {
      login, chatActionCreators, chat,
    } = this.props;
    const {
      chanIsSetup, channels, userList,
    } = this.state;
    if (login[0] && login[0].socket && !chanIsSetup) {
      channels.map((channel) => {
        login[0].socket.on(`#${channel}`, async (data) => {
          const itemChannel = {
            login: data.message.user,
            pseudo: data.message.name,
            avatar: '',
          };
          const user = await this.getUserById(data.message.user);
          itemChannel.avatar = user.avatar;
          chatActionCreators.pushMessage(
            null,
            channel,
            itemChannel,
            data.message.message,
          );
        });
        return channel;
      });
      this.state.chanIsSetup = true;
    }
    chat.map((item) => {
      if (!this.userIsPresent(item.user))userList.push(item.user);
      return item;
    });
  }

  toggleMenu = () => {
    const { showMenu, hideIcon } = this.state;
    this.setState({
      showMenu: !showMenu,
      hideIcon: !hideIcon,
    });
  }

  userIsPresent = (user) => {
    const { userList } = this.state;
    return userList.find(item => (item === user));
  }

  getUserById = async (id) => {
    const { firebase } = this.props;
    return firebase.firestore().collection('users').doc(id)
      .get()
      .then((doc) => {
        if (!doc.exists) {
          return (null);
        }
        return (doc.data());
      })
      .catch(() => (null));
  }

  flushData = async () => {
    const { firebase, login } = this.props;
    firebase.logout().then(() => {
      if (login[0].socket) login[0].socket.close();
    });
  }

  pushMessage = async (message) => {
    const { chatActionCreators, login } = this.props;
    const { currentChannel, user, pseudo } = this.state;
    const { avatar } = await this.getUserById(user);
    chatActionCreators.pushMessage(
      login[0].socket,
      currentChannel,
      {
        login: user,
        pseudo,
        avatar,
      },
      message,
    );
  }

  updateCurrentChannel = (currentChannel) => {
    const { chatActionCreators, firebase } = this.props;
    this.state.currentChannel = currentChannel;
    chatActionCreators.updateProfile(firebase, { currentChannel });
    this.toggleMenu();
  }

  submitLogin = () => {
    const { dispatchSubmitLogin } = this.props;
    dispatchSubmitLogin(this.state);
  }

  render() {
    const { chat, navigation } = this.props;
    const { currentChannel, pseudo } = this.state;

    return (
      <KeyboardAwareScrollView
        style={style.Chat}
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={style.Chat}
        scrollEnabled
      >
        <StatusBar hidden />
        <LinearGradient
          colors={['#9b104f', '#91116d', '#791a7a']}
          style={style.Background}
        />
        <TouchableHighlight style={style.menuIcon} onPress={this.toggleMenu}>
          <Icon name="menu" type="material-community" color="#fff" size={32} />
        </TouchableHighlight>
        <Keyboard pushMessage={this.pushMessage} />
        <View
          style={style.MessageBubbleContainer}
          {...this.panResponder.panHandlers}
        >
          <FlatList
            data={chat.filter(item => (item.room === currentChannel)).reverse()}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => {
              if (item.user !== pseudo) {
                return (<Message styleStr="others" key={item.id} title={item.value} avatar={item.avatar} />);
              }
              return (<Message styleStr="me" key={item.id} title={item.value} avatar={item.avatar} />);
            }}
            inverted
          />
        </View>
        <Menu
          state={this.state}
          flush={this.flushData}
          navigation={navigation}
          updateCurrentChannel={this.updateCurrentChannel}
        />
      </KeyboardAwareScrollView>
    );
  }
}

Chat.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
  login: PropTypes.arrayOf(
    PropTypes.shape({
      username: PropTypes.string.isRequired,
      password: PropTypes.string.isRequired,
      socket: PropTypes.object.isRequired,
    }),
  ).isRequired,
  Firebase: PropTypes.shape().isRequired,
  chatActionCreators: PropTypes.shape({
    pushMessage: PropTypes.func.isRequired,
    updateProfile: PropTypes.func.isRequired,
  }).isRequired,
  dispatchSubmitLogin: PropTypes.func.isRequired,
  chat: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      room: PropTypes.string.isRequired,
      user: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }),
  ).isRequired,
  firebase: PropTypes.shape({
    logout: PropTypes.func.isRequired,
  }).isRequired,
  dispatchLogout: PropTypes.func.isRequired,
};

export default compose(
  connect(),
  firebaseConnect(),
)(Chat);
