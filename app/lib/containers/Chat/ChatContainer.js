import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import ChatBase from './Chat';
import { submitLogin, logout } from '../../actions/login.actions';
import * as ChatActionCreators from '../../actions/chat.actions';

const mapStateToProps = state => ({
  login: state.login,
  chat: state.chat,
  Firebase: state.Firebase,
  Firestore: state.Firestore,
});

const mapDispatchToProps = dispatch => ({
  chatActionCreators: bindActionCreators(ChatActionCreators, dispatch),
  dispatchSubmitLogin: (login) => {
    dispatch(submitLogin(login));
  },
  dispatchLogout: () => {
    dispatch(logout());
  },
});


class Chat extends React.PureComponent {
  render() {
    return (
      <ChatBase {...this.props} />
    );
  }
}

Chat.navigationOptions = {
  header: null,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Chat);
