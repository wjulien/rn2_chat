import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
  Chat: {
    flex: 1,
    flexDirection: 'column',
  },
  Background: {
    margin: 0,
    width: '100%',
    height: '100%',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    marginTop: '0.5%',
    color: '#fff',
    borderRadius: 5,
    height: 45,
    width: '80%',
    padding: 10,
    textAlign: 'center',
    marginBottom: 20,
    backgroundColor: '#000',
    opacity: 0.2,
  },
  menuIcon: {
    position: 'absolute',
    top: 10,
    left: 10,
    zIndex: 1,
  },
  MessageBubbleContainer: {
    position: 'absolute',
    top: 10,
    left: 10,
    width: '95%',
    height: '90%',
    zIndex: 0,
  },
});

export default style;
