import React from 'react';
import { connect } from 'react-redux';

import SignupBase from './Signup';
import { submitSignup } from '../../actions/login.actions';

class Signup extends React.PureComponent {
  render() {
    return (
      <SignupBase {...this.props} />
    );
  }
}

Signup.navigationOptions = {
  header: null,
};

const mapStateToProps = state => ({
  signup: state.signup,
  Firebase: state.Firebase,
});

const mapDispatchToProps = dispatch => ({
  dispatchSubmitSignup: (opt) => {
    dispatch(submitSignup(opt));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Signup);
