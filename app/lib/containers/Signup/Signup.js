/* global Expo */

import React from 'react';
import {
  StatusBar,
  View,
  TextInput,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
import { LinearGradient } from 'expo';
import { Icon } from 'react-native-elements';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import { withNavigation } from 'react-navigation';
import Spinner from 'react-native-loading-spinner-overlay';

import style from './SignupStyle';
import { toaster } from '../../utils';

const initialState = init => ({
  pseudo: init ? init.pseudo : '',
  email: init ? init.email : '',
  password: init ? init.password : '',
  confirmpass: init ? init.confirmpass : '',
  avatar: {},
  loading: false,
});

class Signup extends React.PureComponent {
  constructor(props) {
    super(props);
    const { signup } = this.props;
    this.state = initialState(signup[0]);
  }

  componentWillMount() {
    const { signup } = this.props;
    this.setState(initialState(signup[0]));
  }

  componentWillUnmount() {
    this.setState({ loading: false });
  }

  getAvatar = async () => {
    try {
      const { status } = await Expo.Permissions.getAsync(Expo.Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        Expo.Permissions.askAsync(Expo.Permissions.CAMERA_ROLL).then((res) => {
          if (res.status === 'granted') {
            this.avatarPicker();
          } else {
            Expo.Linking.openURL('app-settings://notification/Expo');
          }
        }).catch(() => {
        });
      } else {
        this.avatarPicker();
      }
    } catch (error) {
      toaster(error.message);
    }
  }

  async avatarPicker() {
    Expo.ImagePicker.launchImageLibraryAsync({
      base64: true,
    }).then((res) => {
      this.setState({
        avatar: {
          uri: res.uri,
          data: res.base64,
        },
      });
    }).catch(() => {
    });
  }

  async submitSignup() {
    const {
      dispatchSubmitSignup,
      navigation,
      Firebase,
    } = this.props;
    const { password, confirmpass } = this.state;

    if (password === confirmpass) {
      this.setState({ loading: true });
      await dispatchSubmitSignup({
        Firebase,
        signup: this.state,
        navigation,
        whenReady: () => { this.setState({ loading: false }); },
      });
    } else {
      toaster('Password and confirmation do not match!');
    }
  }

  render() {
    const {
      avatar, pseudo: ps, email: mail, password: pass, confirmpass: cpass, loading,
    } = this.state;
    const { navigation } = this.props;
    return (
      <View style={style.Signup}>
        <StatusBar hidden />
        <LinearGradient
          colors={['#9b104f', '#91116d', '#791a7a']}
          style={style.Background}
        >
          <Spinner visible={loading} textContent="Loading..." />
          <Text style={style.Title}>Sign Up</Text>
          <View style={style.Logo}>
            {(!avatar)
              ? <Icon name="account-circle" type="material-community" color="#fff" size={92} />
              : (
                <Image
                  style={style.Avatar}
                  source={{ uri: `data:image/png;base64,${avatar.data}` }}
                />
              )}
          </View>
          <TextInput
            returnKeyType="next"
            style={style.text}
            placeholderTextColor="#fff"
            underlineColorAndroid="transparent"
            onChangeText={pseudo => this.setState({ pseudo })}
            placeholder="Pseudo"
            value={ps}
            autoComplete="username"
          />
          <TextInput
            returnKeyType="next"
            style={style.text}
            placeholderTextColor="#fff"
            underlineColorAndroid="transparent"
            onChangeText={email => this.setState({ email })}
            placeholder="Email address"
            value={mail}
            autoComplete="email"
            keyboardType="email-address"
          />
          <TextInput
            returnKeyType="next"
            style={style.text}
            placeholderTextColor="#fff"
            underlineColorAndroid="transparent"
            onChangeText={password => this.setState({ password })}
            placeholder="Password"
            value={pass}
            autoComplete="password"
            secureTextEntry
          />
          <TextInput
            returnKeyType="next"
            style={style.text}
            placeholderTextColor="#fff"
            underlineColorAndroid="transparent"
            onChangeText={confirmpass => this.setState({ confirmpass })}
            placeholder="Confirm Password"
            value={cpass}
            autoComplete="password"
            secureTextEntry
          />
          <TouchableOpacity
            style={style.uploadAvatar}
            onPress={() => this.getAvatar()}
          >
            <Text>Upload Avatar</Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.Sign} onPress={() => this.submitSignup()}>
            <Text>Join Us</Text>
          </TouchableOpacity>

          <View style={style.Home}>
            <Text style={style.textHome} onPress={() => navigation.navigate('Login')}>Back to Home</Text>
          </View>
        </LinearGradient>
      </View>
    );
  }
}

Signup.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
  }).isRequired,
  signup: PropTypes.arrayOf(
    PropTypes.shape({
      pseudo: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired,
      password: PropTypes.string.isRequired,
      confirmpass: PropTypes.string.isRequired,
    }),
  ).isRequired,
  dispatchSubmitSignup: PropTypes.func.isRequired,
  Firebase: PropTypes.shape().isRequired,
};

export default compose(
  connect(),
  firebaseConnect(),
)(withNavigation(Signup));
