import React from 'react';
import { connect } from 'react-redux';

import LoginBase from './Login';
import { submitLogin } from '../../actions/login.actions';

class Login extends React.PureComponent {
  render() {
    return (
      <LoginBase {...this.props} />
    );
  }
}

Login.navigationOptions = {
  header: null,
};

const mapStateToProps = state => ({
  login: state.login,
  Firebase: state.Firebase,
  Firestore: state.Firestore,
});

const mapDispatchToProps = dispatch => ({
  dispatchSubmitLogin: (login) => {
    dispatch(submitLogin(login));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
