import React from 'react';
import {
  StatusBar,
  View,
  TextInput,
  TouchableOpacity,
  Text,
} from 'react-native';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firebaseConnect } from 'react-redux-firebase';
import { LinearGradient } from 'expo';
import { Icon } from 'react-native-elements';
import PropTypes from 'prop-types';
import Spinner from 'react-native-loading-spinner-overlay';

import style from './LoginStyle';
import { toaster } from '../../utils';

const initialState = {
  username: '',
  password: '',
  loading: false,
};

class Login extends React.PureComponent {
  constructor(props) {
    super(props);
    const { Firebase } = props;
    this.state = initialState;
    if (Firebase.profile.isLoaded && !Firebase.profile.isEmpty) {
      this.submitLogin();
    }
  }

  componentWillMount() {
    const { login } = this.props;
    this.setState({
      username: login[0] ? login[0].username : '',
      password: login[0] ? login[0].password : '',
    });
  }

  submitLogin = () => {
    const {
      firebase, Firebase, dispatchSubmitLogin, navigation,
    } = this.props;
    const { username, password } = this.state;

    this.setState({ loading: true });
    try {
      firebase.login({
        email: username, password,
      }).catch((e) => {
        this.setState({ loading: false });
        this.setState(initialState);
        toaster(e.message);
      }).then(async () => {
        if (Firebase.auth.isLoaded && !Firebase.auth.isEmpty) {
          await dispatchSubmitLogin(this.state);
          this.setState(initialState);
          toaster(`Welcome back, ${username}`);
          navigation.navigate({ routeName: 'Chat' });
        }
      });
    } catch (e) { toaster(e); }
  }

  render() {
    const { navigation } = this.props;
    const { username: uname, password: pass, loading } = this.state;


    return (
      <View style={style.Login}>
        <StatusBar hidden />
        <LinearGradient colors={['#9b104f', '#91116d', '#791a7a']} style={style.Background}>
          <Spinner visible={loading} textContent="Loading..." />
          <View style={style.Logo}>
            <Icon name="telegram" type="material-community" color="#fff" size={92} />
          </View>
          <TextInput
            returnKeyType="next"
            style={style.text}
            placeholderTextColor="#fff"
            underlineColorAndroid="transparent"
            placeholder="Email"
            onChangeText={username => this.setState({ username })}
            value={uname}
            autoComplete="username"
            keyboardType="email-address"
          />
          <TextInput
            returnKeyType="next"
            style={style.text}
            placeholderTextColor="#fff"
            underlineColorAndroid="transparent"
            placeholder="Password"
            onChangeText={password => this.setState({ password })}
            value={pass}
            autoComplete="password"
            secureTextEntry
          />
          <TouchableOpacity style={style.LogIn} onPress={() => this.submitLogin()}>
            <Text>Log In</Text>
          </TouchableOpacity>

          <View style={style.SignUp}>
            <Text style={style.textSignUp} onPress={() => navigation.navigate('Signup')}>No account ? Sign up</Text>
          </View>
        </LinearGradient>
      </View>
    );
  }
}

Login.propTypes = {
  login: PropTypes.arrayOf(
    PropTypes.shape({
      username: PropTypes.string.isRequired,
      password: PropTypes.string.isRequired,
      socket: PropTypes.object.isRequired,
    }),
  ).isRequired,
  Firebase: PropTypes.shape().isRequired,
  firebase: PropTypes.shape().isRequired,
  dispatchSubmitLogin: PropTypes.func.isRequired,
  navigation: PropTypes.shape().isRequired,
};

export default compose(
  connect(),
  firebaseConnect(),
)(Login);
