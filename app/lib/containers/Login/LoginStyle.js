import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
  Login: {
    flex: 1,
    flexDirection: 'column',
  },
  Background: {
    margin: 0,
    width: '100%',
    height: '100%',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Logo: {
    marginTop: '-50%',
    marginBottom: '10%',
  },
  Title: {
    marginTop: '-40%',
    color: '#fafafa',
    fontSize: 28,
    fontWeight: '400',
  },
  LogIn: {
    marginTop: 2,
    color: '#000',
    borderRadius: 5,
    height: 45,
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    opacity: 0.8,
  },
  SignUp: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: -1,
    left: 0,
    width: '100%',
    paddingTop: '1%',
    paddingBottom: '3%',
    borderWidth: 0.2,
    borderColor: '#fff',
    opacity: 0.5,
  },
  textSignUp: {
    color: '#fff',
    borderRadius: 5,
    height: 45,
    width: '100%',
    padding: 10,
    textAlign: 'center',
  },
  text: {
    marginTop: '0.5%',
    color: '#fff',
    borderRadius: 5,
    height: 45,
    width: '80%',
    padding: 10,
    textAlign: 'center',
    marginBottom: 20,
    backgroundColor: '#000',
    opacity: 0.2,
  },
  Spinner: {

  },
});

export default style;
