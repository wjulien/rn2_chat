import { connectSocket, pushMessageSocket } from '../services/socket.services';

export const pushSocket = socket => (
  {
    type: 'CONNECT_SOCKET',
    socket,
  }
);

export const pushMessage = (room, message) => (
  {
    type: 'PUSH_MESSAGE',
    room,
    message,
  }
);

export function setupSocket() {
  return dispatch => connectSocket()
    .then((socket) => {
      dispatch(pushSocket(socket));
    });
}

export function fetchPushMessage(socket, room, message) {
  return dispatch => pushMessageSocket(socket, room, message)
    .then(() => {
      dispatch(pushMessage(room, message));
    });
}
