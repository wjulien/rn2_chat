import { pushMessageSocket } from '../services/socket.services';

export function updateProfile(firebase, profile) {
  return () => {
    const idUser = firebase.auth().currentUser.uid;
    firebase.firestore().collection('users').doc(idUser).update(profile);
    return (true);
  };
}

export const createPushMessage = (room, user, message) => (
  {
    type: 'PUSH_MESSAGE',
    room,
    user: user.pseudo,
    avatar: user.avatar,
    value: message,
  }
);

export function pushMessage(socket, room, user, message) {
  return (dispatch) => {
    if (!socket) return dispatch(createPushMessage(room, user, message));
    return pushMessageSocket(socket, room, user, message)
      .then(() => {
        dispatch(createPushMessage(room, user, message));
      });
  };
}

export const setProfile = profile => (
  {
    type: 'UPDATE_PROFILE',
    user: profile.user,
    pseudo: profile.pseudo,
    currentChannel: profile.currentChannel,
    channels: profile.channels,
  }
);
