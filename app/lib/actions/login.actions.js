import uuid from 'uuid';
import firebase from 'firebase';

import { connectSocket } from '../services/socket.services';
import { toaster } from '../utils';

export const pushLogin = (login, socket) => (
  {
    type: 'LOGIN',
    username: login.username,
    password: login.password,
    socket,
  }
);

export const logout = () => ({ type: 'LOGOUT' });

const uploadImageAsync = async (uri) => {
  const blob = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onload = () => {
      resolve(xhr.response);
    };
    xhr.onerror = () => {
      reject(new TypeError('Network request failed'));
    };
    xhr.responseType = 'blob';
    xhr.open('GET', uri, true);
    xhr.send(null);
  });

  const ref = firebase.storage()
    .ref()
    .child(uuid.v4());
  const snapshot = await ref.put(blob);

  blob.close();

  return snapshot.ref.getDownloadURL();
};

export const pushSignup = signup => (
  {
    type: 'SIGNUP',
    email: signup.email,
    pseudo: signup.pseudo,
    password: signup.password,
    confirmpass: signup.confirmpass,
  }
);

export function submitLogin(login) {
  return (dispatch) => {
    connectSocket()
      .then((socket) => {
        dispatch(pushLogin(login, socket));
      });
  };
}

export function submitSignup({
  signup,
  navigation,
  whenReady,
}) {
  const profile = {
    uid: '',
    pseudo: signup.pseudo,
    email: signup.email,
    avatar: '',
    currentChannel: 'general',
    channels: ['general', 'random'],
  };

  return (dispatch) => {
    firebase.createUser({
      email: signup.email,
      password: signup.password,
      displayName: signup.pseudo,
    }).then(async () => {
      profile.uid = firebase.auth().currentUser.uid;
      if (signup.avatar.uri && signup.avatar.uri !== '') {
        profile.avatar = await uploadImageAsync(signup.avatar.uri);
      }

      firebase.firestore().collection('users').doc(profile.uid).set(profile);
      whenReady();
      toaster(`Welcome, ${signup.pseudo}`);
      navigation.navigate({ routeName: 'Login' });
    }).catch((err) => {
      toaster(err.message);
      whenReady();
      dispatch({ type: 'ERROR_SIGNUP' });
    });
  };
}

export const pushProfile = profile => (
  {
    type: 'SET_PROFILE',
    uid: profile.uid,
    user: profile.user,
    pseudo: profile.pseudo,
    currentChannel: profile.currentChannel,
    channels: profile.channels,
  }
);
