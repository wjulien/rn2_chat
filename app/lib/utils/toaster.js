import Toast from 'react-native-root-toast';

export default function (message, opt = {}) {
  Toast.show(message, {
    duration: Toast.durations.LONG,
    position: Toast.positions.TOP,
    shadow: true,
    animation: true,
    hideOnPress: true,
    delay: 0,
    onShow: opt.onShow ? opt.onShow : () => {
      // calls on toast\`s appear animation start
    },
    onShown: opt.onShown ? opt.onShown : () => {
      // calls on toast\`s appear animation end.
    },
    onHide: opt.onHide ? opt.onHide : () => {
      // calls on toast\`s hide animation start.
    },
    onHidden: opt.onHidden ? opt.onHidden : () => {
      // calls on toast\`s hide animation end.
    },
  });
}
