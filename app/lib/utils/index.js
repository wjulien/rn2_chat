import toaster from './toaster';

const version = {
  name: 'red canary',
  code: '1.0.0',
};

export {
  toaster,
  version,
};
