let countMessage = 0;

export default function (state = [], action) {
  switch (action.type) {
    case 'PUSH_MESSAGE':
      countMessage += 1;
      return [
        ...state,
        {
          id: countMessage,
          room: action.room,
          user: action.user,
          value: action.value,
          avatar: action.avatar,
        },
      ];

    default:
      return state;
  }
}
