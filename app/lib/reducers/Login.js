import firebase from 'firebase';

export function login(state = [], action) {
  switch (action.type) {
    case 'LOGIN':
      return [
        {
          username: action.username,
          password: action.password,
          socket: action.socket,
        },
      ];
    case 'LOGOUT':
      firebase.auth().signOut();
      return [
        {
          username: action.username,
          password: action.password,
          socket: action.socket,
        },
      ];

    default:
      return state;
  }
}

export function signup(state = [], action) {
  switch (action.type) {
    case 'SIGNUP':
      return [
        {
          email: action.email,
          pseudo: action.pseudo,
          password: action.password,
          confirmpass: action.confirmpass,
        },
      ];

    default:
      return state;
  }
}
