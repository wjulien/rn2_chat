export default function (state = [], action) {
  switch (action.type) {
    case 'SET_PROFILE':
      return [
        {
          uid: action.uid,
          user: action.user,
          pseudo: action.pseudo,
          currentChannel: action.currentChannel,
          channels: action.channels,
        },
      ];

    default:
      return state;
  }
}
