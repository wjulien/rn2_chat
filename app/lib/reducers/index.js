import { combineReducers } from 'redux';
import { firebaseReducer } from 'react-redux-firebase';
import { firestoreReducer } from 'redux-firestore';
import { login, signup } from './Login';
import profile from './Profile';
import chat from './Chat';

const rootReducer = combineReducers({
  login,
  signup,
  profile,
  chat,
  Firebase: firebaseReducer,
  Firestore: firestoreReducer,
});

export default rootReducer;
