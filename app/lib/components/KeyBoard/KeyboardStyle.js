import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
  MessageInputAndroid: {
    width: '100%',
    position: 'absolute',
    left: 0,
    bottom: 0,
    padding: 10,
    backgroundColor: '#fafafa',
  },
  MessageInputIOS: {
    width: '100%',
    padding: 10,
    backgroundColor: '#fafafa',
  },
});

export default style;
