import React from 'react';
import {
  TextInput,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';

import style from './KeyboardStyle';

class Keyboard extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      message: '',
    };
  }

  setMessage(message) {
    this.setState({ message });
  }

  render() {
    const { message } = this.state;
    const { pushMessage } = this.props;

    return (
      <TextInput
        returnKeyType="send"
        style={(Platform.OS === 'ios') ? style.MessageInputIOS : style.MessageInputAndroid}
        onChangeText={text => this.setMessage(text)}
        blurOnSubmit={false}
        autoFocus={false}
        placeholder="Type a message..."
        value={message}
        onSubmitEditing={() => {
          pushMessage(message);
          this.setMessage('');
        }}
      />
    );
  }
}

Keyboard.propTypes = {
  pushMessage: PropTypes.func.isRequired,
};

export default Keyboard;
