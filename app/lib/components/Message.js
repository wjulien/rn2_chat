import React from 'react';
import {
  Text,
  Image,
  View,
} from 'react-native';
import PropTypes from 'prop-types';

import defaultAvatar from '../../assets/avatar.png';

class Message extends React.PureComponent {
  setStyleBubble() {
    const { styleStr } = this.props;
    return {
      borderTopLeftRadius: 5,
      borderTopRightRadius: 0,
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 5,
      color: (styleStr === 'me') ? '#fafafa' : '#212121',
      alignSelf: (styleStr === 'me') ? 'flex-end' : 'flex-start',
      padding: 10,
      backgroundColor: (styleStr === 'me') ? '#64B5F6' : '#fafafa',
      marginTop: 5,
      marginLeft: 40,
      marginRight: 40,
      marginBottom: 10,
      minWidth: '30%',
      maxWidth: '80%',
      zIndex: 0,
    };
  }

  setStyleAvatar() {
    const { styleStr } = this.props;
    return {
      height: 24,
      width: 24,
      marginTop: -30,
      borderRadius: 50,
      marginRight: 10,
      alignSelf: (styleStr === 'me') ? 'flex-end' : 'flex-start',
      zIndex: 1,
    };
  }

  render() {
    const { title, avatar } = this.props;

    return (
      <View>
        <Text style={this.setStyleBubble()}>
          {title}
        </Text>
        {(typeof avatar === 'string' && avatar !== '') ? (
          <Image
            source={{ uri: avatar }}
            style={this.setStyleAvatar()}
          />
        ) : (
          <Image
            source={defaultAvatar}
            style={this.setStyleAvatar()}
          />
        ) }
      </View>
    );
  }
}

Message.defaultProps = {
  styleStr: 'me',
};

Message.propTypes = {
  styleStr: PropTypes.string,
  title: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
};

export default Message;
