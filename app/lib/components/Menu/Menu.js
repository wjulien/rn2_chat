import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  Image,
} from 'react-native';
import PropTypes from 'prop-types';

import style from './MenuStyle';
import defaultAvatar from '../../../assets/avatar.png';

class Menu extends React.PureComponent {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });
    const { state } = this.props;
    if (!state.channels || state.channels === undefined) {
      state.channels = [];
    }
    const channels = state.channels.map(channel => channel);
    this.state = {
      dataChannels: ds.cloneWithRows(channels),
    };
  }

  componentWillMount() {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });
    const { state } = this.props;
    const channels = state.channels.map(channel => channel);
    this.setState({
      dataChannels: ds.cloneWithRows(channels),
    });
  }

  render() {
    const {
      state,
      updateCurrentChannel,
      flush,
      navigation,
    } = this.props;
    const {
      showMenu,
      pseudo,
      avatar,
      currentChannel,
      userList,
    } = state;

    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });
    const { dataChannels } = this.state;

    if (!showMenu) {
      return null;
    }

    return (
      <View style={style.Menu}>
        {(typeof avatar === 'string' && avatar !== '') ? (
          <Image
            source={{ uri: avatar }}
            style={style.Avatar}
          />
        ) : (
          <Image
            source={defaultAvatar}
            style={style.Avatar}
          />
        ) }
        <Text style={style.User}>
          { pseudo.charAt(0).toUpperCase() + pseudo.slice(1) }
        </Text>
        <Text style={style.Title}>CHANNELS</Text>
        <ListView
          style={style.ListChannels}
          dataSource={dataChannels}
          enableEmptySections
          renderRow={channel => (
            <TouchableOpacity onPress={() => updateCurrentChannel(channel)}>
              <Text style={(channel === currentChannel) ? style.CurrentChannel : style.Channel}>
                {`# ${channel}`}
              </Text>
            </TouchableOpacity>
          )}
        />
        <Text style={style.Title}>USERS</Text>
        <ListView
          style={style.ListChannels}
          dataSource={ds.cloneWithRows(userList)}
          enableEmptySections
          renderRow={user => (
            <TouchableOpacity>
              <Text style={style.Channel}>
                {`#${user}`}
              </Text>
            </TouchableOpacity>
          )}
        />
        <TouchableOpacity onPress={async () => {
          await flush();
          navigation.navigate({ routeName: 'Login' });
        }}
        >
          <Text style={style.Logout}>{('Logout').toUpperCase()}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

Menu.defaultProps = {
  state: {
    channels: [],
    avatar: '',
  },
};

Menu.propTypes = {
  state: PropTypes.shape({
    channels: PropTypes.array,
    userList: PropTypes.array.isRequired,
    avatar: PropTypes.string,
  }),
  updateCurrentChannel: PropTypes.func.isRequired,
  flush: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    dispatch: PropTypes.func.isRequired,
  }).isRequired,
};

export default Menu;
