import SocketIOClient from 'socket.io-client';
import config from '../../config.json';

export async function connectSocket() {
  return new Promise((resolve, reject) => {
    const socket = SocketIOClient(config.url_chat);
    if (!socket) return reject(new Error(`Could not connect to socket at ${config.url_chat}`));
    socket.emit('connection', '');
    return resolve(socket);
  });
}

export async function pushMessageSocket(socket, room, user, message) {
  return new Promise((resolve, reject) => {
    if (!socket) return reject(new Error(`Could not send message ${message} to socket at ${config.url_chat}`));
    socket.emit(`#${room}`, {
      name: user.pseudo,
      user: user.login,
      message,
    });
    return resolve();
  });
}
